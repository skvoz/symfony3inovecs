<?php

namespace AppBundle\Commands;

use AppBundle\Entity\Neos;
use GuzzleHttp\Client;
use GuzzleHttp\Promise\Promise;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Stream;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class NasaCrawler extends ContainerAwareCommand
{
    const API_KEY = 'N7LkblDsc5aen05FJqBQ8wU4qSdmsftwJagVK7UD';

private $client;
    /**
     * NasaCrawler constructor.
     */
    public function __construct()
    {
        $this->client = new  Client();

        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:crawl-nasa')
            ->addOption(
                'period',
                null,
                InputOption::VALUE_OPTIONAL,
                'Example --period=-3 days/month/years',
                "-3 days")
            // the short description shown while running "php bin/console list"
            ->setDescription('Request the data from the last 3 days .')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This must be help');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /*
         *
         * TODO
         * 1. httpclient service
         * 2. create service save data ?
         * 3. add handle exception with scheme response data
         * 4. add work with pagination
         */


        /**
         * ABOUT nasa api
         * Response havent any pagination
         * Response haven next prev period
         * Rate limit 1000 req/h
         * Period cant by more than 7 days, after that use nex prev
         */

        $output->writeln('================================start carwling =================================');
        //http client
        $client = new Client([
            'base_uri' => 'https://api.nasa.gov/neo/rest/v1/'
        ]);

        //generate url
        $endDate = (new \DateTime())->format('Y-m-d');
        if (abs(explode(' ', $input->getOption('period')[0])) > 7) {
            throw new \Exception('Period more than task condition', 422);
        }
        $startDate = (new \DateTime())->modify($input->getOption('period'))->format('Y-m-d');

        $url = 'https://api.nasa.gov/neo/rest/v1/feed?' . http_build_query(['api_key' => self::API_KEY,
            'start_date' => $startDate,
            'end_date' => $endDate]);

        //request to resource
        /** @var Response $response */
        $response = $client->get($url);
        /** @var Stream $body */
        $data = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);

        //save data
        $countRecords = $this->responseSaver($data);

        //summary
        $output->writeln(sprintf('data start: %s', $startDate));
        $output->writeln(sprintf('data end: %s', $endDate));
        $output->writeln(sprintf('count asteroids: %s', $countRecords));
        $output->writeln('================================end carwling =================================');
    }


    /**
     * @param array $responseArray
     * @return int
     */
    protected function responseSaver($responseArray)
    {

        $objectsByDate = $responseArray['near_earth_objects'];
        $order = 0;
        if ($objectsByDate) {

            $em = $this->getContainer()->get('doctrine')->getManager();

            foreach ($objectsByDate as $date => $objects) {
                if (!$objects) {
                    continue;
                }

                foreach ($objects as $object) {
//                    var_dump($object);
//                    die;
                    //persictence
                    $neos = new Neos();
                    $neos->setName($object['name']);
                    $neos->setDate(new \DateTime($date));

                    $neos->setIsHazardous($object['is_potentially_hazardous_asteroid']);

                    $neos->setReference($object['neo_reference_id']);

                    $neos->setSpeed($object['close_approach_data'][0]['relative_velocity']['kilometers_per_hour']);

                    $em->persist($neos);

                    $order++;

                    if ($order % 100) {
                        $em->flush();
                    }
                }
            }

            $em->flush();
        }

        return $order;
    }

}