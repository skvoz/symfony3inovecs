<?php

namespace AppBundle\Services;


use AppBundle\Repository\NeosRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Knp\Component\Pager\Paginator;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class CustomPagination
{
    const LIMIT = 5;
    const PAGE = 1;

    /**
     * @var Container
     */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function response(Query $query, $page = self::PAGE, $limit = self::LIMIT)
    {

        $p = new Paginator;

        /** @var SlidingPagination $view */
        $view = $p->paginate($query->getResult(Query::HYDRATE_ARRAY), $page, self::LIMIT);


        $response = [
            'links' => $this->getLinks('hazardous', $view),
            'data' => $view->getItems(),
        ];

        return $response;
    }

    public function getLinks($route, \Knp\Component\Pager\Pagination\SlidingPagination $pagination)
    {
        $pagData = $pagination->getPaginationData();
        $page = $pagination->getCurrentPageNumber();

        return [
            'self' => $this->generateUrl(
                $route, [
                'page' => $page
            ],
                UrlGeneratorInterface::ABSOLUTE_URL
            ),
            'next' => $this->generateUrl(
                $route, [
                'page' => isset($pagData['next']) ? $pagData['next'] : $page
            ],
                UrlGeneratorInterface::ABSOLUTE_URL
            ),
            'prev' => $this->generateUrl(
                $route, [
                'page' => isset($pagData['prev']) ? $pagData['prev'] : 1
            ],
                UrlGeneratorInterface::ABSOLUTE_URL
            ),
        ];
    }

    private function generateUrl($route, $parameters = array(), $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        return $this->container->get('router')->generate($route, $parameters, $referenceType);
    }
}