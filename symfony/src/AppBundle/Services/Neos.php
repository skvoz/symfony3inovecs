<?php

namespace AppBundle\Services;


use AppBundle\Repository\NeosRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Knp\Component\Pager\Paginator;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class Neos
{
    const LIMIT = 5;

    /**
     * @var Container
     */
    private $container;

    private $em;

    public function __construct(EntityManager $em, Container $container)
    {
        $this->container = $container;
        $this->em = $em;
    }

    /**
     * @return Query
     */
    public function hazardous()
    {
        /** @var NeosRepository $repo */
        $repo = $this->em
            ->getRepository(\AppBundle\Entity\Neos::class);

        /** @var Query $query */
        $query = $repo->findHazardous();

        return $query;
    }

    /**
     * @param bool $isHazardous
     * @return Query
     */
    public function fastes($isHazardous = false)
    {
        /** @var NeosRepository $repo */
        $repo = $this->em
            ->getRepository(\AppBundle\Entity\Neos::class);

        /** @var Query $query */
        $query = $repo->findFastest($isHazardous);

        return $query;
    }

    public function bestYear($isHazardous = false)
    {
        /** @var NeosRepository $repo */
        $repo = $this->em
            ->getRepository(\AppBundle\Entity\Neos::class);

        /** @var Query $query */
        $query = $repo->findBestYear($isHazardous);

        return $query;
    }

    public function bestMonth($isHazardous = false)
    {
        /** @var NeosRepository $repo */
        $repo = $this->em
            ->getRepository(\AppBundle\Entity\Neos::class);

        /** @var Query $query */
        $query = $repo->findBestMonth($isHazardous);

        return $query;
    }
}