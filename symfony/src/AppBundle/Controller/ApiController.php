<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class ApiController
 * @package AppBundle\Controller
 */
class ApiController extends Controller
{
    /*
     * List the rewards of the specified user.
     *
     * This call takes into account all confirmed awards, but not pending or refused awards.
     *
     *@Route("/neo/hazardous", methods={"GET"})
      * @SWG\Response(
     *     response=200,
     *     description="Returns the rewards of an user",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=Reward::class, groups={"full"})
     *     )
     * )
     * @SWG\Parameter(
     *     name="order",
     *     in="query",
     *     type="string",
     *     description="The field used to order rewards"
     * )
     * @SWG\Tag(name="rewards")
     */
    public function hazardousAction(Request $request,
                                    \AppBundle\Services\Neos $neosService,
                                    \AppBundle\Services\CustomPagination $paginationService)
    {
        $page = $request->query->getInt('page', 1);

        $response = $neosService->hazardous();

        return $this->json($paginationService->response($response, $page));
    }

    public function fastesAction(Request $request,
                                 \AppBundle\Services\Neos $neosService,
                                 \AppBundle\Services\CustomPagination $paginationService)
    {
        $page = $request->query->getInt('page', 1);

        $isHazardous = $request->query->getBoolean('hazardous', false);

        $response = $neosService->fastes($isHazardous);

        return $this->json($paginationService->response($response, $page));
    }

    public function bestYearAction(Request $request,
                                   \AppBundle\Services\Neos $neosService,
                                   \AppBundle\Services\CustomPagination $paginationService)
    {
        $page = $request->query->getInt('page', 1);

        $isHazardous = $request->query->getBoolean('hazardous', false);

        $response = $neosService->bestYear($isHazardous);

        return $this->json($paginationService->response($response, $page));
    }

    public function bestMonthAction(Request $request,
                                    \AppBundle\Services\Neos $neosService,
                                    \AppBundle\Services\CustomPagination $paginationService)
    {
        $page = $request->query->getInt('page', 1);

        $isHazardous = $request->query->getBoolean('hazardous', false);

        $response = $neosService->bestMonth($isHazardous);

        return $this->json($paginationService->response($response, $page));
    }

}
