##About
- 0.0.0.0:8000 main page
- 0.0.0.0.8000/api/doc API documentation

## Installation
1. Copy ".env.dist" to ".env"
2. Build/run containers with (with and without detached mode)
   
       ```bash
       $ docker-compose build
       $ docker-compose up -d
       ```
3. Prepare Symfony app
    1. Copy "app/config/parameters.yml.dist" to app/config/parameters.yml", 
    update "database_host" to docker host, also change password, name, username
    (see Useful commands)

        ```yml
        # path/to/your/symfony-project/app/config/parameters.yml
        parameters:
            database_host: db
        ```

    2. Composer install & create database

        ```bash
        $ docker-compose exec php bash
        $ composer install
        $ sf3 doctrine:schema:update --force
        $ sf3 app:crawl-nasa
        ```
4.  Update permission

       ```bash
        $sudo chmod -R 777 symfony/var/cache symfony/var/logs symfony/var/sessions # Symfony3
        $sudo chown -R user:group symfony/var/cache symfony/var/logs symfony/var/sessions # Symfony3
       ```
    
## Useful commands
# Retrieve an IP Address (here for the nginx container)
$ docker inspect --format '{{ .NetworkSettings.Networks.dockersymfony_default.IPAddress }}' $(docker ps -f name=nginx -q)
$ docker inspect $(docker ps -f name=db -q) | grep IPAddress